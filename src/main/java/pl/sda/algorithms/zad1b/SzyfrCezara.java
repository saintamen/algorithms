package pl.sda.algorithms.zad1b;

public class SzyfrCezara {
    private static final int ALPHABET_LENGTH = 26;

    public static String szyfruj(String tekst, int klucz, int direction) {
        char[] tablica = tekst.toCharArray();

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] >= 'A' && tablica[i] <= 'Z') {
                tablica[i] += (klucz * direction);
                if (tablica[i] > 'Z') {
                    tablica[i] -= ALPHABET_LENGTH;
                }
                if (tablica[i] < 'A') {
                    tablica[i] += ALPHABET_LENGTH;
                }
            } else if (tablica[i] >= 'a' && tablica[i] <= 'z') {
                tablica[i] += (klucz * direction);
                if (tablica[i] > 'z') {
                    tablica[i] -= ALPHABET_LENGTH;
                }
                if (tablica[i] < 'a') {
                    tablica[i] += ALPHABET_LENGTH;
                }
            }
        }
        return new String(tablica);
    }


    // 519 088 399
    public static String odszyfruj(String tekst, int klucz, int kierunek) {
        return szyfruj(tekst, klucz, kierunek * (-1));
    }

    public static void main(String[] args) {
        System.out.println(szyfruj("Sample", 12, -1));
        System.out.println(odszyfruj("Goadzs", 12, -1));
    }
}
