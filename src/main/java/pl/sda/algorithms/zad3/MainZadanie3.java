package pl.sda.algorithms.zad3;

import java.util.Scanner;

public class MainZadanie3 {
    // NWD
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj n:");
        int n = scanner.nextInt();

        if (n <= 2) {
            System.err.println("Liczba powinna być większa od 2");
            return; // przerwiemy działanie main'a
        }

        boolean znaleziony = false;
        for (int i = n / 2; i >= 2; i--) {
            if (n % i == 0) {
                // mamy dzielnik
                System.out.println("Dzielnik: " + i);
                znaleziony = true;
                break;
            }
        }

        if(!znaleziony){
            System.out.println("Nie udało się znaleźć dzielnika!");
        }
    }
}
