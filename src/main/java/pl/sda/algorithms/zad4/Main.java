package pl.sda.algorithms.zad4;

public class Main {
    public static void main(String[] args) {
        System.out.println("Silnia: " + silnia(5));
    }

    public static int silnia(int n) {
        if (n > 1) {
            return silnia(n - 1) * n;
        }
        return 1;
    }
}
