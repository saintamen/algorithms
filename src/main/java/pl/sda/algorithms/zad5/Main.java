package pl.sda.algorithms.zad5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<Integer, Integer> elements = new HashMap<>();
        System.out.println("fibo 17: " + fibonacci(30, elements));
    }

    public static int fibonacci(int n, Map<Integer, Integer> elements) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (elements.containsKey(n)) {
            return elements.get(n);
        } else {
            int wynik = fibonacci(n - 1, elements) + fibonacci(n - 2, elements);
            elements.put(n, wynik);
            return wynik;
        }
    }
}
