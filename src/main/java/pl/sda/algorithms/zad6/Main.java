package pl.sda.algorithms.zad6;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random generator = new Random();

        int[] tab = new int[20];
        for (int i = 0; i < 20; i++) {
            tab[i] = generator.nextInt(2000);
        }

        Arrays.sort(tab);
        System.out.println(Arrays.toString(tab));

        Scanner scanner = new Scanner(System.in);
        int szukanaLiczba = scanner.nextInt();
        int pozycja = znajdz(szukanaLiczba, tab, 0, 19);

        for (int i = 0; i < 20; i++) {
            int pozycjaX = znajdz(tab[i], tab, 0, 19);
            System.out.println("Pozycja: " + pozycjaX);
        }

        System.out.println("Pozycja: " + pozycja);
    }

    private static int znajdz(int szukana, int[] tab, int pocz, int kon) {
        if (pocz == kon && tab[kon] != szukana) {
            return -1;
        }
        int srodek = (pocz + kon) / 2;
        if (tab[srodek] > szukana) {
            return znajdz(szukana, tab, pocz, srodek);
        } else if (tab[srodek] < szukana) {
            return znajdz(szukana, tab, srodek + 1, kon);
        }
        return srodek;
    }


}
